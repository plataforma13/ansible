# ansible_parametros_busca_teste
Diretório destinado a estrutura de atualização automática dos parâmetros dos ambientes de teste.

## Arquivos para atualização

> Caso tenha alguma Inclusão de parametro:

Criar params.txt em ./roles/copy_and_exec_files/files/ conforme ex_params.txt dentro de arquivosbusca.

Obs.: dentro do TXT deverá conter apenas o parametro ou os parametros completos separados por quebra de linha.


> Caso tenha alguma mudança de parametro:

Criar script change.sh em ./roles/copy_and_exec_files/files/ conforme ex_change.sh dentro de arquivosbusca.


> Caso tenha alguma query:

Criar query.sql em ./roles/copy_and_exec_files/files/ conforme ex_query.sql dentro de arquivosbusca.

Obs.: Dentro do SQL deverá conter apenas a query ou as querys completas separadas por quebra de linha.



## Ativar a atualização

Após colocar os arquivos no local indicado acima, basta fazer a release no busca para que o jenkins chame o ansible.
